;Marowak by Txpot

[Data]
life = 324
attack = 100
defence = 100
power = 3000
fall.defence_up = 50
liedown.time = 60
airjuggle = 15
sparkno = 2
guard.sparkno = 40
KO.echo = 0
volume = 0
IntPersistIndex = 60
FloatPersistIndex = 40

[Size]
xscale = 1           ;Horizontal scaling factor.
yscale = 1           ;Vertical scaling factor.
ground.back = 15     ;Player width (back, ground)
ground.front = 16    ;Player width (front, ground)
air.back = 12        ;Player width (back, air)
air.front = 12       ;Player width (front, air)
height = 60          ;Height of player (for opponent to jump over)
attack.dist = 160    ;Default attack distance
proj.attack.dist = 90 ;Default attack distance for projectiles
proj.doscale = 0     ;Set to 1 to scale projectiles too
head.pos = -5, -90   ;Approximate position of head
mid.pos = -5, -60    ;Approximate position of midsection
shadowoffset = 0     ;Number of pixels to vertically offset the shadow
draw.offset = 0,0    ;Player drawing offset in pixels (x, y). Recommended 0,0

[Velocity]
walk.fwd  = 2      ;Walk forward
walk.back = -2.2     ;Walk backward
run.fwd  = 4.6, 0    ;Run forward (x, y)
run.back = -4.5,-3.8 ;Hop backward (x, y)
jump.neu = 0,-8.4    ;Neutral jumping velocity (x, y)
jump.back = -2.55    ;Jump back Speed (x, y)
jump.fwd = 2.5       ;Jump forward Speed (x, y)
runjump.back = -2.55,-8.1 ;Running jump speeds (opt)
runjump.fwd = 4,-8.1      ;.
airjump.neu = 0,-8.1      ;.
airjump.back = -2.55      ;Air jump speeds (opt)
airjump.fwd = 2.5         ;.
air.gethit.groundrecover = -.15,-3.5  ;Velocity for ground recovery state (x, y) **MUGEN 1.0**
air.gethit.airrecover.mul = .5,.2     ;Multiplier for air recovery velocity (x, y) **MUGEN 1.0**
air.gethit.airrecover.add = 0,-4.5    ;Velocity offset for air recovery (x, y) **MUGEN 1.0**
air.gethit.airrecover.back = -1       ;Extra x-velocity for holding back during air recovery **MUGEN 1.0**
air.gethit.airrecover.fwd = 0         ;Extra x-velocity for holding forward during air recovery **MUGEN 1.0**
air.gethit.airrecover.up = -2         ;Extra y-velocity for holding up during air recovery **MUGEN 1.0**
air.gethit.airrecover.down = 1.5      ;Extra y-velocity for holding down during air recovery **MUGEN 1.0**

[Movement]
airjump.num = 1       ;Number of air jumps allowed (opt)
airjump.height = 35   ;Minimum distance from ground before you can air jump (opt)
yaccel = .74          ;Vertical acceleration
stand.friction = .85  ;Friction coefficient when standing
crouch.friction = .82 ;Friction coefficient when crouching
stand.friction.threshold = 2          ;If player's speed drops below this threshold while standing, stop his movement **MUGEN 1.0**
crouch.friction.threshold = .05       ;If player's speed drops below this threshold while crouching, stop his movement **MUGEN 1.0**
air.gethit.groundlevel = 25           ;Y-position at which a falling player is considered to hit the ground **MUGEN 1.0**
air.gethit.groundrecover.ground.threshold = -20 ;Y-position below which falling player can use the recovery command **MUGEN 1.0**
air.gethit.groundrecover.groundlevel = 10   ;Y-position at which player in the ground recovery state touches the ground **MUGEN 1.0**
air.gethit.airrecover.threshold = -1  ;Y-velocity above which player may use the air recovery command **MUGEN 1.0**
air.gethit.airrecover.yaccel = .35    ;Vertical acceleration for player in the air recovery state **MUGEN 1.0**
air.gethit.trip.groundlevel = 15      ;Y-position at which player in the tripped state touches the ground **MUGEN 1.0**
down.bounce.offset = 0, 20            ;Offset for player bouncing off the ground (x, y) **MUGEN 1.0**
down.bounce.yaccel = .4               ;Vertical acceleration for player bouncing off the ground **MUGEN 1.0**
down.bounce.groundlevel = 12          ;Y-position at which player bouncing off the ground touches the ground again **MUGEN 1.0**
down.friction.threshold = .05         ;If the player's speed drops below this threshold while lying down, stop his movement **MUGEN 1.0**

[Quotes]
victory0 = "!"

;=============================================================================
;=============================================================================
;==================== Variable Usage =========================================

; Variables can be split into two groups: system and user.

; System Variables:
; System variables are used by the system and shouldn't be modified unless otherwise stated.
;
; var(0) = ATB Gauge Max
; 	 - This is the maximum value that the ATB bar is at any time.
; 	 - This changes and is set at the very beginning of a move.
; var(1) = ATB Gauge Current Value
;	 - This is the current value the ATB bar has at any time.
;	 - DON'T modify this at all. The system handles it all times.
; var(2) = ATB Gauge Modifier
;	 - This value can be set within a move to change the recovery rate of the ATB Gauge.
;	 - For example, Scyther's Agility sets this value to increase the rate of recovery for 3 moves. After those three moves are up, it is set back to 0.
;	 - Don't set this to a ridiculously high number because it's used like this:
;		- current_value := current_value + 1 + var(2)
; var(3) = AI Attack Index.
;	 - When the AI chooses an attack, it's stored in this variable.
;	 - Don't modify this value.
; var(4) = Status Effect
;	 - 1 = Burn
;	 - 2 = Paralysis
;	 - 3 = Poison
;	 - 4 = Sleep
;	 - 5 = Freeze
;	 - 6 = Bad Poison
;	 - 7 = Confusion
;	 - 8 = Curse
; var(5) = Attacking Enemy's ID
;	 - Used for reading data from attack enemy
;	 - Don't modify this value.
; var(6) = Total Damage Done (per hit)
;	 - Post calculated damage amount
;	 - Don't change this value unless you have post-calculated damage you intend on showing, e.g. recoil damage
; var(7) = AI Attack Threshold
;	 - Used to count ticks before the AI can attack. Currently counts to 200 before the enemy can attack.
;	 - Don't modify this value.
; var(8) = Special Move
;	 - If this is set to 1, the move in question is "Special" and will use Special Attack and Special Defense stats instead of Attack and Defense
; var(9) = Move's Accuracy
; var(30) = Miss Flag
;	  - 1 if the move is calculated to miss; 0 otherwise
; var(31) = Crit Hit Flag
;	  - 1 if the current move is a crit; 0 otherwise
; var(36) = Total Calculated Damage (of total hits before recover)
;	  - This isn't in use yet, but might be in the future. The var does store the right value though.
; fvar(0) = Player position.
;	  - At the beginning of the round, this value is recorded so the pokemon always return to their spots after behing hit away from them, or moving away during an attack.
;	  - Don't modify this.
; fvar(1) = Current Damage Modifier Value
;	  - This is used for affinites.
;	  - Don't modify this value.
; fvar(2) = Critical Rate
;	  - This is the critical rate percentage
; fvar(3) = Crit Damage Modifier
;	  - 1.5 if crit triggered; 1.0 otherwise
; fvar(4) = Attack Stat
; fvar(5) = Special Attack Stat
; fvar(6) = Defense Stat
; fvar(7) = Special Defense Stat
; fvar(8) = Accuracy Stat
; fvar(9) = Evasion Stat
; fvar(10~29) = Type weakness rates
; fvar(30) = Calcuated Hit Rate
;
; User Variables:
; User variables are variables that you can use for anything in your character. There's not much restriction here. You can use these for whatever you want. In most cases, for advanced attacks, you'll
; need these.
; var(40+)
; fvar(40+)

;---------------------- USER -----------------------------------------------
;--------------  Character-specific  ---------------------------------------

; Var(40): Palette 4-6 Mode On?
; Var(41): Bone Rush Max Hits
; Var(42): Bone Rush Hits
; Var(43): Focus Energy Boost
; Var(44):
; Var(45): Thrash Max Hits
; Var(46):

;=============================================================================
;==================== BUG FIXING TO DO ==================================

;1.
