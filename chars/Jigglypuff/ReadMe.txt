﻿                            ____________________________________________
===========================| Jigglypuff by Trinitroman AKA Trinitronity |===========================
                            ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯       [25.09.15]
=====<Features>=====

 - All the essential stuff for Ryon's and Alexei's Project Catch'em all plus some of Txpot's improvements
 - Competitive as a Special Ability
 - moves mechanics accurate to source game

=====<Movelist>=====

.Rollout:
->Starts out weak and slow. However, with every hit this move does without missing or getting damage, it gets faster and stronger, but also gets more and more cooldown. The fifth hit is the strongest.
.Pound:
->A quick slap which deals moderate damage.
.Sing:
->Jigglypuff's first signature move. It starts to sing a lullaby, which makes the opponent fall asleep for between 1-5 rounds.
.Rest:
->Jigglypuff's second signature move. It falls asleep for two rounds, but restores its health completely.

=====<Competitive>=====

 - Unlike in the source game, Jigglypuff's attack deal twice as much damage while around 1/4 of its health or lower.

=====<Version History>=====

<v.16/10/14>
 - First release

=====<Special Thanks>=====
 - Joshr for the original Flareon sprites (I did one sprite edit)
 - Ryon for Project Catch'em All
 - Alexei for Project Catch'em All and for the kanji FX during Fire Blast
 - Bulbapedia for stats, so this Jigglypuff can be extremely source accurate
 - Txpot for nice little quirks like Critical Hit
 - Jmorphman for his readme template, which I have used for this readme
 - Nintendo and Game Shark for making POKéMON happen
 - Elecbyte for making MUGEN happen
 - MUGEN Fighters Guild Forums for its high quality feedback

=====<Disclaimer>=====

 - The Jigglypuff character(?) is property of Nintendo and Game Shark
 - The original POKèMON games are property of Nintendo and Game Shark
 - This MUGEN character is a non-profit fan work, it cannot be used for any commercial purposes