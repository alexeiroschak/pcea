; The CMD File
[Remap]
x = x
y = y
z = z
a = a
b = b
c = c
s = s

;-| Default Values |-------------------------------------------------------
[Defaults]
command.time = 1
command.buffer.time = 1

;-| Single Button |---------------------------------------------------------
[Command]
name = "a"
command = a

[Command]
name = "b"
command = b

[Command]
name = "c"
command = c

[Command]
name = "x"
command = x

[Command]
name = "y"
command = y

[Command]
name = "z"
command = z

[Command]
name = "start"
command = s

[Command]
name = "up"
command = U

[Command]
name = "down"
command = D

;-| Required |--------------------------------------------------------------
[Command]
name = "holdfwd"
command = /$F

[Command]
name = "holdback"
command = /$B

[Command]
name = "holdup"
command = /$U

[Command]
name = "holddown"
command = /$D

[Command]
name = "FF"
command = F, F
time = 10

[Command]
name = "BB"
command = B, B
time = 10

;---------------------------------------------------------------------------
[Statedef -1]

[State -1, Placeholder]
type = Null
trigger1 = 0