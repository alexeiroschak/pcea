[Croagunk's README]

IMPORTANT:
This character is for a mugen game called Project Catch 'Em All. DON'T expect it work properly in any other installation.

Name: Croagunk
Series: Pokemon
Description: Croagunk is a primarily poison, fighting frog Pokemon who packs a punch infused with poison whenever he gets the chance.
Credits: 
- JoshR961 for the original idea of the animated Pokemon sprites.
- Daeron for creating the custom Croagunk sprites.
- TxPot for his status indicators and critical hit logic.
	- I changed it a bit to make it more dynamic, but the original is still by him.

Abilities:
Poison Touch: All physical moves that make contact have a 30% chance to poison the enemy. This effect does not stack.

Critical Hits: All moves have a 12.5% chance to deal double damage via critical hit. This damage multiplier is applied after all other damage modifiers. AI opponents have a 33.33% chance to critically hit.

Legend:
[Move Name:] [Power Damage] / [Type] / [Speed Rating]
Description
Notes

Pursuit: 40 / Dark / Fast
Croagunk lunges at the enemy quickly dealing a small amount of dark damage that knocks back.
Useful for interrupts and getting a quick poison on the enemy. This move applies to Poison Touch.

Poison Jab: 80 / Poison / Medium
Croagunk cloaks his fist in poison and dashes towards the enemy in an attempt to poison them with the hit.
This move has a 30% chance to poison the enemy regardless of Poison Touch.

Brick Break: 75 / Fighting / Medium
Croagunk jumps in to the air and slams his hand down, cutting the enemy. The move is strong enough break bricks.
This move applies to Poison Touch.

Venoshock: 65 / Poison / Slow
Croagunk fills the glands on his cheeks with a thick gooey poison that he sprays at the enemy. It drenches and damages for a medium amount of damage.
If the enemy is already poisoned, this move does double damage.