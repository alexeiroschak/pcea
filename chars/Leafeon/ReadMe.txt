﻿                            _________________________________________
===========================| Leafeon by Trinitroman AKA Trinitronity |===========================
                            ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯        [12.01.15]
=====<Features>=====

 - All the essential stuff for Ryon's and Alexei's Project Catch'em all plus some of Txpot's improvements
 - Leaf Guard as a Special Ability
 - moves mechanics accurate to source game

=====<Movelist>=====

.Quick Attack:
->A high priority move, which is super fast.
.Leaf Blade:
->A powerful slash with the back tail. It has a high critical-hit ratio.
.Sunny Day:
->Leafeon changes the weather and lets the sun shine brightly. Powers up Synthesis, but also powers up FIRE-type attacks.
.Synthesis:
->Leafeon recovers some of its health. The amount of health that gets recovered is determined by the weather conditions.

=====<Leaf Guard>=====

 - During sunny weather, Leafeon is immune to all status effects.

=====<Version History>=====

<v.12/01/15>
 - First release

=====<Special Thanks>=====
 - Joshr for the original Flareon sprites (I did one sprite edit)
 - Ryon for Project Catch'em All
 - Alexei for Project Catch'em All
 - Shinrei for some frames during Leaf Blade
 - ??? for the Leaf Blade tail trail FX
 - Bulbapedia for stats, so this Flareon can be extremely source accurate
 - Txpot for nice little quirks like Critical Hit
 - Jmorphman for his readme template, which I have used for this readme
 - Nintendo and Game Shark for making POKéMON happen
 - Elecbyte for making MUGEN happen
 - MUGEN Fighters Guild Forums for its high quality feedback

=====<Disclaimer>=====

 - The Leafeon character(?) is property of Nintendo and Game Shark
 - The original POKèMON games are property of Nintendo and Game Shark
 - This MUGEN character is a non-profit fan work, it cannot be used for any commercial purposes