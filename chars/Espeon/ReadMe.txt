﻿                            ________________________________________
===========================| Espeon by Trinitroman AKA Trinitronity |===========================
                            ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯        [29.12.14]
=====<Features>=====

 - All the essential stuff for Ryon's and Alexei's Project Catch'em all plus some of Txpot's improvements
 - Synchronize as a Special Ability
 - moves mechanics accurate to source game

=====<Movelist>=====

.Quick Attack:
->A high priority move, which is super fast.
.Psychic:
->A full-screen attack, which highly damages the opponent, although Espeon has to charge it up first and cool down afterwards.
.Future Sight:
->Espeon first shoots a sphere into the air. If the opponent attack 3 times after that, a bolt will strike the opponent and deal damage.
.Morning Sun:
->Espeon recovers some of its health. The amount of health that gets recovered is determined by the weather conditions.

=====<Synchronize>=====

 - If Espeon is burned, paralyzed or poisoned by another POKéMON, that POKéMON will be inflicted with the same status condition.

=====<Version History>=====

<v.xx/xx/15>
 - various hitbox corrections
 - bug fixes to Synchronize
 - longer startup for Psychic

<v.29/12/14>
 - First release

=====<Special Thanks>=====
 - Joshr for the original Espeon sprites (I did one sprite edit)
 - Ryon for Project Catch'em All
 - Alexei for Project Catch'em All
 - Bulbapedia for stats, so this Espeon can be extremely source accurate
 - Txpot for nice little quirks like Critical Hit
 - Jmorphman for his readme template, which I have used for this readme
 - Nintendo and Game Shark for making POKéMON happen
 - Elecbyte for making MUGEN happen
 - MUGEN Fighters Guild Forums for its high quality feedback

=====<Disclaimer>=====

 - The Espeon character(?) is property of Nintendo and Game Shark
 - The original POKèMON games are property of Nintendo and Game Shark
 - This MUGEN character is a non-profit fan work, it cannot be used for any commercial purposes