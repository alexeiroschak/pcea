﻿                            _________________________________________
===========================| Umbreon by Trinitroman AKA Trinitronity |===========================
                            ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯       [16.03.15]
=====<Features>=====

 - All the essential stuff for Ryon's and Alexei's Project Catch'em all plus some of Txpot's improvements
 - Synchronize as a Special Ability
 - moves mechanics accurate to source game

=====<Movelist>=====

.Quick Attack:
->A high priority move, which is super fast.
.Feint Attack:
->Umbreon fakes an attack, only to attack the opponent from behind. Unavoidable, but needs start-up and hes cooldown
.Dark Pulse:
->Umbreon creates circles of dark energy, which harm the opponent. 20% chance of flinching.
.Moonlight:
->Umbreon recovers some of its health. The amount of health that gets recovered is determined by the weather conditions.

=====<Synchronize>=====

 - If Umbreon is burned, paralyzed or poisoned by another POKéMON, that POKéMON will be inflicted with the same status condition.

=====<Version History>=====

<v.16/03/15>
 - First release

=====<Special Thanks>=====
 - Joshr for the original Espeon sprites (I did one sprite edit)
 - Ryon for Project Catch'em All
 - Alexei for Project Catch'em All
 - Bulbapedia for stats, so this Espeon can be extremely source accurate
 - Txpot for nice little quirks like Critical Hit
 - Jmorphman for his readme template, which I have used for this readme
 - Nintendo and Game Shark for making POKéMON happen
 - Elecbyte for making MUGEN happen
 - MUGEN Fighters Guild Forums for its high quality feedback

=====<Disclaimer>=====

 - The Umbreon character(?) is property of Nintendo and Game Shark
 - The original POKèMON games are property of Nintendo and Game Shark
 - This MUGEN character is a non-profit fan work, it cannot be used for any commercial purposes