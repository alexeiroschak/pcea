﻿                            _________________________________________
===========================| Glaceon by Trinitroman AKA Trinitronity |===========================
                            ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯        [24.12.15]
=====<Features>=====

 - All the essential stuff for Ryon's and Alexei's Project Catch'em all plus some of Txpot's improvements
 - Ice Body as a Special Ability
 - moves mechanics accurate to source game

=====<Movelist>=====

.Quick Attack:
->A high priority move, which is super fast.
.Ice Shard:
->A high priority move, where Glaceon shoots out ice chunks super fast.
.Blizzard:
->Glaceon fires a stream of icy wind, which can hit the opponent and rarely even freeze the opponent. During Hail, Blizzard turns into a full-screen attack that always hits.
.Hail:
->Glaceon changes the weather into a cold hail. Hurts POKéMON that are not ICE-type, but also triggers Glaceon's Ice Body ability and powers up Blizzard.

=====<Ice Body>=====

 - During Hail, Glaceon will regain 1/16 of its maximum HP every turn.

=====<Version History>=====

<v.24/12/15>
 - First release (on Christmas, baby)

=====<Special Thanks>=====
 - Joshr for the original Glaceon sprites
 - Ryon for Project Catch'em All
 - Alexei for Project Catch'em All
 - GarchompMatt for his sound rips used for Glaceon's Blizzard
 - Bulbapedia for stats, so this Glaceon can be extremely source accurate
 - Txpot for nice little quirks like Critical Hit
 - Jmorphman for his readme template, which I have used for this readme
 - Nintendo and Game Shark for making POKéMON happen
 - Elecbyte for making MUGEN happen
 - MUGEN Fighters Guild/ MUGEN Free For All Forums for its high quality feedback

=====<Disclaimer>=====

 - The Glaceon character(?) is property of Nintendo and Game Shark
 - The original POKèMON games are property of Nintendo and Game Shark
 - This MUGEN character is a non-profit fan work, it cannot be used for any commercial purposes