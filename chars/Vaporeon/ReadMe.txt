﻿                            __________________________________________
===========================| Vaporeon by Trinitroman AKA Trinitronity |===========================
                            ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯       [18.10.14]
=====<Features>=====

 - All the essential stuff for Ryon's and Alexei's Project Catch'em all plus some of Txpot's improvements
 - Water Absorb as a Special Ability
 - moves mechanics accurate to source game

=====<Movelist>=====

.Quick Attack:
->A high priority move, which is super fast.
.Surf:
->Vaporeon dashes forward while creating a big wave of water, which can damage the opponent.
.Hydro Pump:
->Vaporeon shoots out a stream of water, which hits the opponent so fast that the opponent gets damaged. High damage, but also high cooldown.
.Aqua Ring (non-damaging):
->Vaporeon casts watery rings. Afterwards, 1/16 of Vaporeon's maximum health gets restored after every move for the rest of the battle.

=====<Water Absorb>=====

 - If Vaporeon is hit by water-type attacks, part of the damage will be converted into health.

=====<Version History>=====

<v.21/10/14>
 - First release

=====<Special Thanks>=====
 - Joshr for the original Vaporeon sprites (I did a few sprite edits)
 - Ryon for Project Catch'em All
 - Alexei for Project Catch'em All
 - Bulbapedia for stats, so this Vaporeon can be extremely source accurate
 - Txpot for nice little quirks like Critical Hit
 - Jmorphman for his readme template, which I have used for this readme
 - Nintendo and Game Shark for making POKéMON happen
 - Elecbyte for making MUGEN happen
 - MUGEN Fighters Guild Forums for its high quality feedback

=====<Disclaimer>=====

 - The Vaporeon character(?) is property of Nintendo and Game Shark
 - The original POKèMON games are property of Nintendo and Game Shark
 - This MUGEN character is a non-profit fan work, it cannot be used for any commercial purposes