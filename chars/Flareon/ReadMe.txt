﻿                            _________________________________________
===========================| Flareon by Trinitroman AKA Trinitronity |===========================
                            ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯        [18.10.14]
=====<Features>=====

 - All the essential stuff for Ryon's and Alexei's Project Catch'em all plus some of Txpot's improvements
 - Flash Fire as a Special Ability
 - moves mechanics accurate to source game

=====<Movelist>=====

.Quick Attack:
->A high priority move, which is super fast.
.Flare Blitz:
->Flareon engulfs itself in flames and charges at the opponent. High damage and 10% chance for burn, but Flareon receives recoil damage as well.
.Fire Blast:
->Flareon shoots out a flame, which shapes itself into a fiery 大 kanji. High damage and 10% chance for burn, but it might miss the opponent.
.Flamethrower:
->Flareon breaths a stream of flames. 10% chance that the opponent gets burn injuries

=====<Flash Fire>=====

 - If Flareon is hit by fire-type attacks, Flareon's fire attacks deal 1.5x damage.

=====<Version History>=====

<v.18/10/14>
 - changed the way, how Flamethrower is done (thanks, Txpot)
 - added a patch for Quilava, which changes his Flamethrower to Flareon's version
 - changed Flareon's 9000_2 portrait (thanks, GarchompMatt)
 - updated and fixed hurt hitboxes
 - added this one readme

<v.16/10/14>
 - First release

=====<Special Thanks>=====
 - Joshr for the original Flareon sprites (I did one sprite edit)
 - Ryon for Project Catch'em All
 - Alexei for Project Catch'em All and for the kanji FX during Fire Blast
 - Bulbapedia for stats, so this Flareon can be extremely source accurate
 - Txpot for nice little quirks like Critical Hit
 - Jmorphman for his readme template, which I have used for this readme
 - Nintendo and Game Shark for making POKéMON happen
 - Elecbyte for making MUGEN happen
 - MUGEN Fighters Guild Forums for its high quality feedback

=====<Disclaimer>=====

 - The Flareon character(?) is property of Nintendo and Game Shark
 - The original POKèMON games are property of Nintendo and Game Shark
 - This MUGEN character is a non-profit fan work, it cannot be used for any commercial purposes