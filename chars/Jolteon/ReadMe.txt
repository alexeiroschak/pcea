﻿                            _____________________________________________________
===========================| Jolteon by Trinitroman AKA Trinitronity & QuickFist |===========================
                            ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯        [24.10.14]
=====<Features>=====

 - All the essential stuff for Ryon's and Alexei's Project Catch'em all plus some of Txpot's improvements
 - Volt Absorb as a Special Ability
 - moves mechanics accurate to source game

=====<Movelist>=====

.Quick Attack:
->A high priority move, which is super fast.
.Agility:
->Jolteon's next 2 attacks will have a lot less cooldown.
.Zap Cannon:
->Jolteon shoots out a spark, which causes a lot of damage and immidiately paralyzes the opponent, but it might miss the opponent.
.Wild Charge:
->Jolteon engulfs itself in electric sparks and dashes forward. The damage is high, but Jolteon receives recoil damage as well.

=====<Volt Absorb>=====

 - If Jolteon is hit by electric-type attacks, part of the damage will be converted into health.

=====<Version History>=====

<v.xx/xx/15>
 - corrected Jolteon's Quick Attack hitbox

<v.24/10/14>
 - First release

=====<Special Thanks>=====
 - Joshr for the original Jolteon sprites (I did one sprite edit)
 - Ryon for Project Catch'em All
 - Alexei for Project Catch'em All
 - QuickFist for his original Jolteon
 - Bulbapedia for stats, so this Jolteon can be extremely source accurate
 - Txpot for nice little quirks like Critical Hit
 - Jmorphman for his readme template, which I have used for this readme
 - Nintendo and Game Shark for making POKéMON happen
 - Elecbyte for making MUGEN happen
 - MUGEN Fighters Guild Forums for its high quality feedback

=====<Disclaimer>=====

 - The Jolteon character(?) is property of Nintendo and Game Shark
 - The original POKèMON games are property of Nintendo and Game Shark
 - This MUGEN character is a non-profit fan work, it cannot be used for any commercial purposes