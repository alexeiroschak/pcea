
=====================
= Metallic Corridor =
=====================

By Txpot

Version: 1.0

#Version Dates#
-----------------------------------------------------
27th October 2014: Created and released as version 1.0


=====================================================

#INTRODUCTION#

A corridor with metal walls. It can be used to show off Electric, Steel, and other pokemon
that might have came from a lab or factory of some sort.

It has two versions. The Wide version extends the stage as far as possible without causing
graphical glitches with the Attack Menu.

I hope that you have fun with this creation, as I did making it!

By the way, if you are interested in using some of this creation's stuff, you can use
whatever you want. There is no need to ask, since this creation was created using free
resources anyway. All you need to do is credit me as well as the original authors.

#Credits#

Here are the people I thank for letting me use, or I helped myself to, their coding,
sprites, and whatnot. 

JoshR691: Custom sprites from Spriter's Resource.
Alexei: Feedback.

And You: For downloading this character! If you have any suggestions, comments, and bug
reports, please drop a line sometime. 

Email address is txpot1@gmail.com!

If you would like to see more of my creations, go to my website below this sentence!

http://txpotstuff.t15.org/index.html

See ya!